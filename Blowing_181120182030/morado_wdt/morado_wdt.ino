// MORADO_WDT
// VERSION 181120182030

// TX23           MAC {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED } IP(10, 10, 10, 1);

// BLOWING - 1    MAC {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEE } IP(10, 10, 10, 2); morado //OSC /blowing/1
// BLOWING - 2    MAC {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEF } IP(10, 10, 10, 3); naranja //OSC /blowing/2
// BLOWING - 3    MAC {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xF0 } IP(10, 10, 10, 4); rojo //OSC /blowing/3
// BLOWING - 4    MAC {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xF1 } IP(10, 10, 10, 5); amarillo //OSC /blowing/4

#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>
#include <FastCRC.h>
#include <Bounce2.h>
#include "OSCMessage.h"

#define   FACTOR_UP               20                // 2.0 % percent increments
#define   FACTOR_DOWN             30                // 3.0 % percent decrements

#define   TIME_SEND_OSC           500               // Send OSC Message every 200 milliseconds
#define   MAX_VALUE_OSC           1000              // 1000 to maintain software compatibility

#define   PHOTO_SENSOR            2
#define   LED_PWM_1               22
#define   LED_PWM_2               23

#define   INDUC_SENSOR            3

#define   TRANSITION_DIMMER       5
#define   DIMMER_MAX              235
#define   DIMMER_MIN              0

#define   MAX_SPEED_MOTOR         850
#define   MAX_TIME_MOTOR          180000             // 60 seconds max without activity


EthernetUDP UDP;

// HOST Configuration
byte MAC[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEE };
IPAddress IP(10, 10, 10, 2);
const unsigned int inPort = 9001;                     // Incoming Port

// Server Configuration
IPAddress Host_IP(10, 10, 10, 100);
const unsigned int outPort = 8000;                    // Outgoing Port

int speed = 0;
int speed_old = 0;
int map_speed = 0 ;

FastCRC16 CRC16;

uint8_t motorShutdown[]  = {0x02,0x06,0x21,0x35,0x00,0x06};
uint8_t motorSwitchOn[]  = {0x02,0x06,0x21,0x35,0x00,0x07};
uint8_t motorForward[]   = {0x02,0x06,0x21,0x35,0x00,0x0F};
uint8_t motorReverse[]   = {0x02,0x06,0x21,0x35,0x08,0x0F};
uint8_t motorSetSpeed[]  = {0x02,0x06,0x14,0xA1,0x01,0xF4};
uint8_t motorQuickStop[] = {0x02,0x06,0x21,0x35,0x00,0x02};

enum motor {
  STOP,
  FORWARD,
  REVERSE,
  SETSPEED,
  SHUTDOWN,
  QUICKSTOP  
};

Bounce debouncer = Bounce();

void setup() {
  
  pinMode(9, OUTPUT);
  digitalWrite(9, LOW);   // end reset pulse
  delay(100);
  digitalWrite(9, HIGH);    // begin reset the WIZ820io

  pinMode(LED_PWM_1, OUTPUT);
  pinMode(LED_PWM_2, OUTPUT);  

  for (unsigned char i = 0; i < 5 ; i++)
  {  
  digitalWrite(LED_PWM_1, LOW);
  digitalWrite(LED_PWM_2, HIGH);
  delay(200);
  digitalWrite(LED_PWM_1, HIGH);
  digitalWrite(LED_PWM_2, LOW);
  delay(200);
  }
  digitalWrite(LED_PWM_1, HIGH);
  digitalWrite(LED_PWM_2, HIGH);

  // Motor INIT
  Serial3.begin(19200);
  ctlMotor(SHUTDOWN,0);
  ctlMotor(STOP,0);
  delay(200);
  ctlMotor(FORWARD,  0);
  ctlMotor(SETSPEED, 0);

  pinMode(PHOTO_SENSOR,INPUT_PULLUP);

  debouncer.attach(INDUC_SENSOR,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  debouncer.interval(25); // Use a debounce interval of 25 milliseconds
  
  Serial.println("Initialize Ethernet with STATIC IP:");
  Ethernet.begin(MAC, IP);
  if (Ethernet.hardwareStatus() == EthernetNoHardware) 
  Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");

  // Check for Cable Ethernet present
  if (Ethernet.linkStatus() == LinkOFF) 
  Serial.println("Ethernet cable is not connected.");
    
  Serial.println(Ethernet.localIP());
  
  UDP.begin(inPort);

  noInterrupts();                                         
  WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
  WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
  delayMicroseconds(1);                                   // Need to wait a bit..

  //1 second WDT timeout
  WDOG_TOVALH = 0x006d;
  WDOG_TOVALL = 0xdd00;

  // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
  WDOG_PRESC  = 0x400;

  // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
  WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE | WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN | WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
  interrupts();  

}

double        average = 0;
int           count = 0;
unsigned char sensor = 0;

elapsedMillis send_OSC_watch = 0;
elapsedMillis motor_watch = 0;

unsigned long previousMillis = 0;
unsigned int  dimm_value = DIMMER_MIN;
unsigned int  dimm_sense = 0;

void led_dimmer(void)
{
  if( millis()-previousMillis < TRANSITION_DIMMER) return;
  else
  { 
    if(dimm_sense)
    {
      if(dimm_value == DIMMER_MAX) dimm_sense = 0;
      else
      dimm_value ++; 
    }
    else
    {
      if(dimm_value == DIMMER_MIN) dimm_sense = 1;      
      else
      dimm_value --; 
    }    
  }
  analogWrite(LED_PWM_1,dimm_value);  
  analogWrite(LED_PWM_2,dimm_value);  
  previousMillis = millis();
}

unsigned char motor_up    = 0;

void resetWDT()
{
noInterrupts();                                     //   No - reset WDT
WDOG_REFRESH = 0xA602;
WDOG_REFRESH = 0xB480;
interrupts();
}

void loop(){

if(digitalReadFast(PHOTO_SENSOR)) sensor = 0;
else sensor = 1;
debouncer.update();

if(debouncer.fell() && sensor) {motor_up = 1; motor_watch = 0;}
if(sensor == 0 || motor_watch > MAX_TIME_MOTOR) motor_up = 0;

if(motor_up)
{
  if(speed + FACTOR_UP >= MAX_SPEED_MOTOR) speed = MAX_SPEED_MOTOR;
  else speed = speed + FACTOR_UP;
}
else
{
  if(speed - FACTOR_DOWN <= 0) speed = 0;
  else speed = speed - FACTOR_DOWN;
}

if(speed_old != speed)
{
ctlMotor(SETSPEED, speed);
Serial.println("MOTOR : " + (String)speed);
}
speed_old = speed;

led_dimmer();

if(send_OSC_watch > TIME_SEND_OSC)
{
  send_OSC_watch = 0;
  send_OSC();
}

resetWDT();
}


void send_OSC()
{   
  if(Ethernet.hardwareStatus() != EthernetNoHardware && Ethernet.linkStatus() != LinkOFF)
  {
    map_speed = map(speed, 0, MAX_SPEED_MOTOR, 0, MAX_VALUE_OSC);

    OSCMessage msg("/blowing/1");
    msg.add(map_speed);
    UDP.beginPacket(Host_IP, outPort);
    msg.send(UDP);                      // send the bytes to the SLIP stream
    UDP.endPacket();                    // mark the end of the OSC Packet
    msg.empty();                        // free space occupied by message

    Serial.print("Speed in OSC Message : ");
    Serial.println(map_speed);    
   }

}


void ctlMotor(unsigned char cmd, int value)
{
  delay(60);           //!

  switch(cmd)
  {
    case STOP     : sendModbus(motorSwitchOn); 
                    break;

    case FORWARD  : sendModbus(motorForward); 
                    break;

    case REVERSE  : sendModbus(motorReverse); 
                    break;

    case SETSPEED : motorSetSpeed[4] = highByte(value);
                    motorSetSpeed[5] = lowByte(value);    
                    sendModbus(motorSetSpeed); 
                    break;

    case SHUTDOWN : sendModbus(motorShutdown); 
                    break;

    case QUICKSTOP: sendModbus(motorQuickStop); 
                    break;
  
  }
}

void sendModbus(uint8_t *bufTemp)
{
  uint16_t crc = CRC16.modbus(bufTemp, 6);
  Serial3.write(bufTemp,6);
  Serial3.write(lowByte(crc));
  Serial3.write(highByte(crc));
}
